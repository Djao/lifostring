/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream>
#include <cstring>  

using namespace std;
string ConcatRemove(string s, string t, int k);

int main()
{
    string s = "";
    string t= "";
    int k = 1;
    cin.get();
    getline(cin, s);
    cin.get();
    getline(cin, t);
    cin>>k;
    
    cout<<ConcatRemove(s,t,k)<<endl;
    
    return 0;
}

string ConcatRemove(string s, string t, int k) {
    int ls = s.length();
    int lt = t.length();
    bool r;
    if(k >= ls + lt) return "yes";
    int i;
    
    for (i = 0; i < ls && i < lt; i++) {
        if(s[i] != t[i]) break;
    }
    
    return (k % ((ls + lt - 2 * i) != 0?(ls + lt - 2 * i):2) == 0)?"yes":"no";
}
