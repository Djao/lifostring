# Question 3

## Description
Write a function that takes 3 parameters  
- s: starting string
- t: desired string
- k: an integer that represents the number of operations  
And returns if it's possible to change string 's' in string 't' only by removing/adding to the last position of 's'.

## Live-Test
In this link [https://onlinegdb.com/-RvtzZ2k8](https://onlinegdb.com/-RvtzZ2k8) it's possible to run the solution online and test values for s, t and k.  
- The first line contains the string s, the starting string.
- The second line contains the string t, the desired string.
- The third line contains an integer k, the number of operations.

For example  
abc  
def  
6  
yes  

abc  
def  
5  
no  

## Validation
If 'k' is bigger than or equal the sum of 's' and 't' lengths then the result is always "yes", due to the possibility of removing all elements from starting string and adding all elements of desired string after.  
Otherwise we calculate the minimum amount of actions needed to remove undesired characters from input and to add characters from desired string. That is done by comparing how many characters both strings have in common at the start, if the starting string is equal to the desired string then we consider 2 as the minimum amount of meaningful steps, which means removing and adding the same character. For 'k' to be a valid amount of steps it must be a multiple of the mininum amount of steps, so we calculate (k mod steps).
## Comments

